from imagekit import ImageSpec
from imagekit.processors import ResizeToFit, ResizeToFill, Anchor, SmartCrop, SmartResize
from PIL import Image


class DynamicSpec(ImageSpec):
    format = 'JPEG'
    options = {'quality': 60}

    def __init__(self, source, width, height):
        self.width = width
        self.height = height
        self.source = source
        super(ImageSpec, self).__init__()

    @property
    def processors(self):
        return [SmartResize(self.width, self.height)]


class ImageUtils:

    def get_pil_image_from_path(self, path):
        return Image.open(path)

    def get_image_from_path(self, path, open_mode='rb'):
        return open(path, open_mode)

    def convert_image(self, image_obj, width, height):
        try:
            image_generator = DynamicSpec(image_obj, width, height)
            result = image_generator.generate()
            content = result.read()
        except OSError:
            content = ''

        return content

    def convert_image_from_path(self, path, width, height):
        image_obj = self.get_image_from_path(path)
        return self.convert_image(image_obj, width, height)
