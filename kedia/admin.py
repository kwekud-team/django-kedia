from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from kedia.models import Picture
from kedia.attrs import PictureAttr


@admin.register(Picture)
class SettingAdmin(admin.ModelAdmin):
    list_display = PictureAttr.admin_list_display
    list_filter = PictureAttr.admin_list_filter
    search_fields = ['section']
    list_editable = ['is_active', 'sort']


class PictureInline(GenericTabularInline):
    model = Picture
    fields = ['image', 'section', 'description', 'sort']
    extra = 1