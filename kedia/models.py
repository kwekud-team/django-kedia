import logging

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.safestring import mark_safe

from versatileimagefield.fields import VersatileImageField, PPOIField


class PictureMixin:
    picmix_image_field = 'image'
    picmix_thumb_size = '50x50'

    def get_html_thumbnail(self):
        img_field = getattr(self, self.picmix_image_field)
        url = self.get_image_url(img_field, self.picmix_thumb_size)
        if url:
            html = f'<img src="{url}" />'
            return mark_safe(html)
        return ''
    get_html_thumbnail.short_description = 'Thumbnail'

    def get_image_url(self, img_field, size):
        try:
            url = img_field.thumbnail[size].url
        except Exception as e:
            logging.error(str(e))
            url = ''
        return url


class Picture(models.Model, PictureMixin):
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    sort = models.PositiveIntegerField(default=1000)

    object_guid = models.UUIDField(null=True, blank=True)
    object_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    content_object = GenericForeignKey()

    section = models.CharField(max_length=50, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    external_url = models.URLField(null=True, blank=True)

    image = VersatileImageField(upload_to='kedia/picture/image/', width_field='width', height_field='height',
                                ppoi_field='ppoi')
    height = models.PositiveIntegerField(blank=True, null=True)
    width = models.PositiveIntegerField(blank=True, null=True)
    ppoi = PPOIField()

    def __str__(self):
        return str(self.pk)

    class Meta:
        ordering = ('sort',)

    @staticmethod
    def get_picture_qs(content_object):
        if content_object:
            qs = Picture.objects.filter(object_id=content_object.pk,
                                        content_type__app_label=content_object._meta.app_label,
                                        content_type__model=content_object._meta.model_name)
        else:
            qs = Picture.objects.none()

        return qs

    def save(self, *args, **kwargs):
        if self.content_object:
            self.object_guid = getattr(self.content_object, 'guid', None)

        super(Picture, self).save(*args, **kwargs)
