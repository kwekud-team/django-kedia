from django import template

from kedia.models import Picture

register = template.Library()


@register.filter
def get_object_pictures(content_obj):
    return Picture.get_picture_qs(content_obj)
