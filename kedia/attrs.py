

class PictureAttr:
    admin_list_display = ['pk', 'content_object', 'section', 'image', 'is_active', 'sort', 'object_guid']
    admin_list_filter = ['is_active']

